#!/bin/bash
##1. Enabled production mode.
##2. Minified js, CSS, HTML.
##3. Merged js and CSS.
##4. Enabled Redis and varnish cache.
##5. Enabled gzip and leverage cache.

## check gzip compression curl -s -H "Accept-Encoding: gzip" -I https://hvs13.hyperx.cloud insecure | grep -i Content-Encoding
# SED following to enable gzip , but magent sample.conf already contains this 
#gzip on;
#gzip_disable "msie6";

#gzip_comp_level 6;
#gzip_min_length 1100;
#gzip_buffers 16 8k;
#gzip_proxied any;
#gzip_types
#    text/plain
#    text/css
#    text/js
#    text/xml
#    text/javascript
#    application/javascript
#    application/x-javascript
#    application/json
#    application/xml
#    application/xml+rss
#    image/svg+xml;
#gzip_vary on;

## Add these lines to .conf for perticualr host for enable leverage browser caching in conf.d/
## sed command to insert line after last line 
##`$ sed '$ a This is the last line' sedtest.txt
#location ~* \.(js|jpg|jpeg|gif|png|css|tgz|gz|rar|bz2|doc|pdf|ppt|tar|wav|bmp|rtf|swf|ico|flv|txt|woff|woff2|svg)$ {
#etag on;
#if_modified_since exact;
#add_header Pragma "public";
#add_header Cache-Control "max-age=31536000, public";
#}
#Credientials
read -p 'Enter username of domain: ' uservar
echo the operation will perform on $uservar'.com'

#minify js css html for hvs13 only
echo "extension=php_intl.so" >> /home/$uservar/etc/php7.2/php.ini
/home/$uservar/public_html/bin/magento cache:enable
/home/$uservar/public_html/bin/magento deploy:mode:set production
/home/$uservar/public_html/bin/magento config:set dev/js/merge_files 1 
/home/$uservar/public_html/bin/magento config:set dev/js/minify_files 1
/home/$uservar/public_html/bin/magento config:set dev/css/merge_css_files 1
/home/$uservar/public_html/bin/magento config:set dev/css/minify_files 1
/home/$uservar/public_html/bin/magento config:set dev/js/enable_js_bundling 1
/home/$uservar/public_html/bin/magento config:set dev/template/minify_html 1
/home/$uservar/public_html/bin/magento setup:upgrade
/home/$uservar/public_html/bin/magento setup:di:compile
/home/$uservar/public_html/bin/magento setup:static-content:deploy
chmod 755 -R /home/$uservar/public_html/generated
chmod 755 -R /home/$uservar/public_html/var 
chmod 755 -R /home/$uservar/public_html/pub 
/home/$uservar/public_html/bin/magento cache:flush
# Enable javascript bundling
/home/$uservar/public_html/bin/magento config:set dev/js/enable_js_bundling 1
#merge  js files
/home/$uservar/public_html/bin/magento config:set dev/js/minify_files 0
/home/$uservar/public_html/bin/magento config:set dev/static/sign 0
/home/$uservar/public_html/bin/magento config:set dev/js/merge_files 1
/home/$uservar/public_html/bin/magento setup:static-content:deploy 
/home/$uservar/public_html/bin/magento cache:clean 
#merge css
/home/$uservar/public_html/bin/magento config:set dev/css/merge_css_files 0
/home/$uservar/public_html/bin/magento config:set dev/css/minify_files 0
/home/$uservar/public_html/bin/magento config:set dev/css/merge_css_files 1
/home/$uservar/public_html/bin/magento setup:static-content:deploy
/home/$uservar/public_html/bin/magento cache:clean
# next
sudo yum install epel-release > /dev/null
sudo yum install redis -y > /dev/null
sudo systemctl enable redis
sudo systemctl start redis
#make backup of env.php
cp -a /home/$uservar/public_html/app/etc/env.php /home/$uservar/
# set magento to use redis
echo
echo "Y" | /home/$uservar/public_html/bin/magento setup:config:set --session-save=redis --session-save-redis-host=127.0.0.1 --session-save-redis-db=1 > /dev/null
echo
echo "maxmemory = [2048M];" >> /etc/redis.conf


##Add these lines to .conf for perticualr host for enable leverage browser caching in conf.d/
sed -i '$ a server {' /etc/nginx/conf.d/$uservar.conf
sed -i "$ a server_name $uservar.hyperx.cloud" /etc/nginx/conf.d/$uservar.conf
sed -i '$ a location ~* \.(js|jpg|jpeg|gif|png|css|tgz|gz|rar|bz2|doc|pdf|ppt|tar|wav|bmp|rtf|swf|ico|flv|txt|woff|woff2|svg)$ {' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a etag on;' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a if_modified_since exact;' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a add_header Pragma "public";' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a add_header Cache-Control "max-age=31536000, public";' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a }' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a }' /etc/nginx/conf.d/$uservar.conf

## enable gzip for magento in .conf file of host
sed -i '$ a gzip on;' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a gzip_disable "msie6";' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a gzip_comp_level 6;' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a gzip_min_length 1100;' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a gzip_buffers 16 8k;' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a gzip_proxied any;' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a gzip_types' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a 	text/plain' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a 	text/css' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a 	text/js' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a 	text/xml' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a 	text/javascript' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a 	application/javascript' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a 	application/x-javascript' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a 	application/json' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a 	application/xml' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a 	application/xml+rss' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a 	image/svg+xml;' /etc/nginx/conf.d/$uservar.conf
sed -i '$ a gzip_vary on;' /etc/nginx/conf.d/$uservar.conf
##sed -i '$ a }' /etc/nginx/conf.d/$uservar.conf
 
